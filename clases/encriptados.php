<?php
class Encriptados{
		private $palabra;
		private $clave;
        private $encriptado;
    public function __construct($palabra,$clave,$encriptado){
        $this->palabra = $palabra;
        this->clave = $clave;
        this->encriptado = $encriptado;
    }

	public function getPalabra(){ return $this->palabra; }
	public function setPalabra($palabra){ $this->palabra = $palabra; }

	public function getClave(){ return $this->clave; }
    public function setClave($clave){ $this->clave = $clave; }	
    
    public function getEncriptado(){ return $this->encriptado; }
	public function setEncriptado($encriptado){ $this->encriptado = $encriptado; }
    
    public function __toString(){
        return $this->palabra." ".$this->clave." ".$this->encriptado;
    }
    
    
}	
?>