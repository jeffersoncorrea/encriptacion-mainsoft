<?php
include_once('model/model.php');
header("Content-Type: apllication/json");// Se define que el contenido que va a devolver la pagina es Json.
switch($_SERVER['REQUEST_METHOD']){ // Evalua el tipo de peticion que se realizando a la aplicaciones(GET,POST,PUT,DELETE,ETC).
    case 'POST':
        $_POST = json_decode(file_get_contents('php://input'),true); //Almacena el archivo Json en la variable.
        $palabraEncriptada = encriptado($_POST['palabra'],$_POST['clave']); //se envian los datos del Json a la funcion de encriptado.
        $json = [];
        $json = ['encriptado'=>$palabraEncriptada];
        echo json_encode($json);// se imprime la respuesta en formato JSON.
        break;
}
?>