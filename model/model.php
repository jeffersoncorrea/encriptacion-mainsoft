<?php
function encriptado($palabra,$clave){ //Los parametros obtenidos vienen del archivo api.
    
    $abecedario = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]; //Se esta definiendo el arreglo sobre el cual se van a encriptar las contraseñas o palabras.
    $tamañoAbc=count($abecedario); //evalua el tamaño del arreglo
    
    $palabraArreglo = str_split($palabra); //Guarda la palabra que se desea encriptar como tipo array y cada letra se convierte en una posición.
    $tamañoPalabra=count($palabraArreglo); //Evalua el tamaño de la palabra a encriptar
    $cadena = "";
    for($i=0;$i<$tamañoPalabra;$i++){ //Realiza el recorrido de la palabra a encriptar segun el tamaño del arreglo
        $buscarLetraPalabra = array_search($palabraArreglo[$i],$abecedario,true); // La funcion array_search() realiza la busqueda de una letra indicada($palabraArreglo[$i]) "en este caso una posicion dentro del arreglo llamado($palabraArreglo)" en un arreglo($abecedario) y devulve la posicion de la misma.
        $posicionLetraEncriptada = $buscarLetraPalabra+$clave; // Suma la $clave a la posición obtenida en el paso anterior.
        $operacion = modulado($posicionLetraEncriptada,$tamañoAbc); //Envia la información a la funcion modulado().
        $cadena .= $abecedario[$operacion]; //Concatena los resultados del arreglo($abecedario) en cada una de las posiciones devueltas por la función modulado(). 
    }
        return $cadena;
}

function modulado($posicionLetraEncriptada,$tamañoAbc){// funcion encargada de realizar la validacion e inicio del recorrido del arreglo para encriptado.
    $operacion = $posicionLetraEncriptada % $tamañoAbc; // obtiene el residuo de dividir la posición ya encriptada($posicionLetraEncriptada) en el tamaño del arreglo($abecedario) sobre el cual se va a encriptar.
    if($operacion!=0){// si el residuo($operacion) es diferente de cero la variable $operacionx sera igual que el residuo de lo contrario sera igual a la posicion de la letra encriptada - 1 posicion.
            $operacionx=$operacion;
        }else{
            $operacionx=$posicionLetraEncriptada-1;
        }
        return $operacionx;//variable de retorno
}

function generacionJson(){
    
}
?>